import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateDevicePage } from './update-device';
import { TranslateModule } from '@ngx-translate/core';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    UpdateDevicePage
  ],
  imports: [
    IonicPageModule.forChild(UpdateDevicePage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ]
})
export class UpdateDevicePageModule {}
