import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PoiListPage } from './poi-list';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    PoiListPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(PoiListPage),
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ],
})
export class PoiListPageModule {}
